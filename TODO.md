# TODOs for the Tundra Wolf project

This document contains a number of TODOs for the project. The tasks listed here are:

* Large changes that require significant restructuring of the code.
* Small changes that are unimportant right now, but do need to be implemented.

## Large Changes

### Add logging (tw-errors)

* Added: 14-Dec-2023
* Target version: 0.1

Add logging to the tw-errors crate, so it can be used for all end-user and development status reporting. The requirement is to have 4 levels of messages (INFO, WARNING, ERROR, FATAL ERROR) and depending on what the user requests provide the level indicated.

Fatal errors should always be reported and logged. Other messages can be either logged or reported or both. Logging can be set to screen or file.

The format for log files is AsciiDoc, but it can be converted to HTML so the user can analyse the messages and take action accordingly.

### Refactor semantic model (tw-data / analyzer plugins / tw-semrep)

* Added: 14-Dec-2023
* Target version: 0.9

The semantic model for the project has to contain the following information.

* Project semantics (conditional compilation, code generated in the build process, ...)
* Source code semantics (behaviour of the compiled project)

Basically the semantic model is build by static/semantic analysis of both the project build process and the resulting source code.

The result, end state, will be stored in a graph that is defined in the crate `tw-semrep`, but some of the structures are now in the crate `tw-data` and the analysis plugin crates. A generalized, complete and consistent graph needs to be includes in the `tw-semrep` crate.

## Small Changes

### Convenience macros for error reporting (tw-errors)

* Added: 03-Jan-2024
* Target version: 0.1

Add convenience macros for error reporting that add extra information to the error reported. The following formats will be used by error type.

* Analyzer/Generator/Refactorer Errors

For file sources:

    [<command>-<phase>:<severity>-<code>:<file>:<line>:<column>] <message>
    in <function/method>
    at <source-code-line>
       <error-indicators>

For other sources:

    [<command>-<phase>:<severity>-<code>:<source-id>:<line>:<column>] <message>
    in <function/method>
    at <source-code-line>
       <error-indicators>

* Source Errors

    [source:<severity>-<code>:<source-id>] <message>
    in <function/method>

* Internal Error

    {<severity>-<code>:<function>:<source>:<line>} <message>

* User Error

    [<severity>-<code>:<user>:<function>] <message>

This will help debugging issues if the users report the errors they receive.

### Build script - Add timing of each step

* Added: 02-Jan-2024
* Target version: 0.2

Add a bit of timing information to the build script, so at least the devs have an idea on how long it takes to run each step and the total build cycle.

### Build script - Version number, information dump on error

* Added: 26-Dec-2023
* Target version: 0.1

Currently the build script has no version number. In order to make reporting of issues with the build easier, that needs to be added. For the same reason we also need to be able to dump information on the build (using the environment variable `TUNDRA_WOLF_DUMP_BUILD`).

### Build script - Use `[PROJECT_PATH\]` argument

* Added: 25-Dec-2023
* Target version: 0.2

Currently the build scripts will only perform their task if run from the project root directory. This change is to add the `PROJECT_PATH` argument and make sure the build script can be launched and works correctly from any directory within or outside of the source tree.

Also part of this change is to move all build files outside of the source tree. This requires 1) making sure the build script can still find all required scripts and files and 2) Cargo builds in a `target` directory outside of the source tree.

## Applied Changes

### Split project in different crates with each it's own repository

* Added: 14-Dec-2023
* Completed: 20-Dec-2023
* Target version: 0.1

Currently the project lives in a single Mercurial repository. The idea is to split it into a respository by crate, so the crates can be published seperately in crates.io.

GitLab/SourceHut look promising for this exercise.
