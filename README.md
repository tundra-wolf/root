# README for the Tundra Wolf project

The Tundra Wolf project provides tools for analysing, refactoring and converting software projects. The toolset is easily extendable to support other languages/technologies by allowing the creation of plugins for analysing, refactoring and generating different languages based on the internal semantic format to which input projects are converted.

The initial focus is on converting software projects in different languages/technologies to the Rust programming language. The first languages/technologies supported are the following.

* Source
    * C
    * Make
* Target
    * Rust

For version 0.1 the project will support the following additional technologies.

* Source
    * AsciiDoc
    * ASP.NET
    * HTML
    * Markdown
    * SQL
    * Visual Basic.NET
* Target
    * HTML
    * MongoDB
    * PostgreSQL

## Installation

## Getting Started

## Project Overview

The project consists of the following components. The initial priorities are marked with a ~ character.

* Binaries
    * ~twc - A command line/scripting tool for automating project processing.
    * ~twd - A command line tool for processing documentation.
    * twg - A graphical IDE, mainly for analysis and refactoring of projects.
    * twt - A text based IDE, with the same functionality as the graphical IDE.
* Libraries
    * ~tw-analyzer - Shared components used by all analyzer plugins.
    * tw-data - Common data structures used by all binaries, libraries and plugins.
    * ~tw-errors - Error reporting and logging components.
    * ~tw-plugin - Shared components for all plugins.
    * ~tw-generator - Shared components used by all generator plugins.
    * tw-refactorer - Shared components used by all refactorer plugins.
    * ~tw-semrep - Semantic representation of a software project.
    * tw-shell - Interactive shell using Rune syntax.
    * ~tw-source - Source handling used by all components.
* Plugins
    * Analyzer Plugins
        * twa-c - C programming language
        * twa-cpp - C/C++ preprocessor language
        * twa-csharp - C# programming language
        * ~twa-css - CSS (and derivatives) layout language
        * twa-cxx - C++ programming language
        * twa-documentdb - Document DB database and query languages
        * twa-graphdb - Graph DB database and query languages
        * ~twa-html - HTML (and derivatives) markup language
        * twa-java - Java programming language
        * ~twa-javascript - Javascript (and ECMA) scripting languages
        * ~twa-markeup - Documentation markup languages
        * twa-pb - Powerbuilder workspaces/projects
        * ~twa-projects - IDE project structure/configuration
        * twa-rust - Rust programming language
        * twa-shell - Shell scripting languages
        * ~twa-sql - SQL database and query languages
        * twa-text - Text processing languages
        * ~twa-vb - Visual Basic programming language
        * ~twa-xml - XML (and derivatives) markup language
    * Generator Plugins
        * ~twg-documentation - Documentation markup languages
        * ~twg-documentdb - Document DB database and query languages
        * twg-graphdb - Graph DB database and query languages
        * ~twg-html - HTML5/CSS markup language
        * ~twg-json - JSON configuration language
        * ~twg-rust - Rust programming language
        * ~twg-toml - TOML configuration language
    * Refactorer Plugins
        * twr-rename - Rename objects in source code
