#
# build.sh - UNIX build script
#

#
# This script contains some shortcuts to configura, build and test the project. It also,
# when allows the developer to run a continuous integration cycle on the project.
#
# Usage:
#   build.sh <command> [options] [PROJECT_PATH]
#
# Commands:
#   add     Add a new package, binary, test, example, ...
#   build   Build all, a package, a binary, a library, ...
#   ci      Continuous integration
#   doc     Documentation maintenance functions
#   run     Run a binary, an example, ...
#   test    Run tests
#
# Flags:
#   -h, --help  Display help information and exit
#
# Variables:
#   PROJECT_PATH    Project root path
#

#---
# Continuous integration cycle
#---
# The continuous integration cycle performs the following tasks.
#
#   > At program start
#       - Remove conflict files (created by Dropbox)
#       - Remove stackdump files (created by PowerShell, when SSH connection fails)
#       - Remove .DS_Store files (created by MacOSX)
#       - Cleanup temporary files (in target/tmp)
#       - Read the configuration file (.ci.<hostname>.toml in the project root)
#       - If the CI log path doesn't exist, create it
#       - Read or set the cycle count (ci_path/CYCLE_COUNT)
#
#   > CI cycle (repeats indefinitely)
#       - Get packages
#       - For each package found
#           - Run CARGO BUILD
#               - If an error occurs, run and display build information
#           - Run CARGO TEST
#               - If an error occurs, run tests and display results
#       - [TODO] Run integration tests
#       - Run user specified tests (ci_path/USER_SPECIFIED_TESTS)
#       - Increase and save cycle count in ci_path/CYCLE_COUNT
#---

BUILD_SCRIPT_VERSION="0.0.1"

script_path=$(cd $(dirname $0) | pwd)

#
# Paths
#
ci_log_path="$script_path/target/ci"

tmp_path="$script_path/target/tmp"
if [ ! -d $tmp_path ]
then
    mkdir -p $tmp_path
fi

tools_path="$script_path/tools"
tools_build_path="$tools_path/build"

#
# Constants
#

# Main executable package
readonly MAIN_EXE_PKG="tundra-wolf"

# Plugin types
readonly PI_ANALYZER=1
readonly PI_GENERATOR=2
readonly PI_REFACTORER=3

# Test types
readonly T_INTEGRATION_TEST=1
readonly T_BENCHMARK=2
readonly T_UNIT_TEST=3

#
# Includes
#
. $tools_build_path/functions.sh
. $tools_build_path/add_command.sh
. $tools_build_path/doc_command.sh

#---
# Get packages
#---
typeset -A packages

readonly PT_BINARY=1
readonly PT_LIBRARY=2
readonly PT_ANALYZER=3
readonly PT_GENERATOR=4
readonly PT_REFACTORER=5

typeset -A package_types

function get_packages
{
    # Get library packages
    for package_path in $(find . -type d -depth 2 | grep tw- | grep -v target)
    do
        package=${package_path##*/}
        if [[ "$package" =~ tw-* ]]
        then
            packages[$package]=$package_path
            package_types[$package]=$PT_LIBRARY
        fi
    done

    # Get plugin packages (analyzers)
    for package_path in $(find . -type d -depth 3 | grep twa- | grep -v target)
    do
        package=${package_path##*/}
        if [[ "$package" =~ twa-* ]]
        then
            packages[$package]=$package_path
            package_types[$package]=$PT_ANALYZER
        fi
    done

    # Get plugin packages (generators)
    for package_path in $(find . -type d -depth 3 | grep twg- | grep -v target)
    do
        package=${package_path##*/}
        if [[ "$package" =~ twg-* ]]
        then
            packages[$package]=$package_path
            package_types[$package]=$PT_GENERATOR
        fi
    done

    # Get plugin packages (refactorers)
    for package_path in $(find . -type d -depth 3 | grep twr- | grep -v target)
    do
        package=${package_path##*/}
        if [[ "$package" =~ twr-* ]]
        then
            packages[$package]=$package_path
            package_types[$package]=$PT_REFACTORER
        fi
    done

    # Get binary packages
    for package_path in $(find . -type d -depth 1 | grep tundra-wolf | grep -v target)
    do
        package=${package_path##*/}
        packages[$package]=$package_path
        package_types[$package]=$PT_BINARY
    done
}

#---
# Read CI Configuration File
#---
default_ci_config=$script_path/.ci.toml
ci_config=$script_path/.ci.$(hostname).toml

cycle_delay=60
error_delay=60
package_delay=5
ci_path=$ci_log_path
cargo_params=""

function read_ci_config
{
    if [ -z $1 ]
    then
        echo "[Error] No file provided for function 'read_ci_config'"
        exit 1
    fi

    # cycle-delay
    tmp=$(cat $1 | grep ^cycle-delay | cut -d = -f 2 | tr -d "[:space:]")
    if [ -z $tmp ]
    then
        cycle_delay=60
    else
        cycle_delay=$tmp
    fi

    # error-delay
    tmp=$(cat $1 | grep ^error-delay | cut -d = -f 2 | tr -d "[:space:]")
    if [ -z $tmp ]
    then
        error_delay=60
    else
        error_delay=$tmp
    fi

    # package-delay
    tmp=$(cat $1 | grep ^package-delay | cut -d = -f 2 | tr -d "[:space:]")
    if [ -z $tmp ]
    then
        package_delay=60
    else
        package_delay=$tmp
    fi

    # ci-path
    tmp=$(cat $1 | grep ^ci-path | cut -d = -f 2 | tr -d "[:space:]" | tr -d "\"")
    if [ -z $tmp ]
    then
        ci_path=$ci_log_path
    else
        ci_path=$tmp
        ci_log_path=$tmp
    fi

    cargo_params="--config build.target-dir=\"$ci_path/..\""

    return 0
}

#---
# Test Create Command (webload)
#---
function t_create_webload_project
{
    test_cmd = "twc"

    # include configuration settings
    test_cmd_args = "create --project-db ./tundra-wolf/examples/projects.toml --projects-path ./tundra-wolf/examples/projects --projects-source-path ./tundra-wolf/examples/projects/source --copy-source webload ./tundra-wolf/examples/projects/source/webload"

    # no configuration settings, with project type
    test_cmd_args = "create --project-type visual-studio webload ./tundra-wolf/examples/projects/source/webload"

    # no configuration settings, no project type
    test_cmd_args = "create webload ./tundra-wolf/examples/projects/source/webload"
}

#---
# Main program
#---
ci=0

readonly NO_COMMAND=0

cmd=$NO_COMMAND

readonly CMD_ADD=1
readonly CMD_BUILD=2
readonly CMD_DOC=3
readonly CMD_RUN=4
readonly CMD_TEST=5

if [ -z "$@" ]
then
    ci=1
else
    args_processed=0

    # parse command (has to be the first argument)
    case $1 in
        ci)
            ci=1
            ;;

        add)
            cmd=$CMD_ADD
            shift
            get_packages

            do_add_args $@
            if [[ $? -ne 0 ]]
            then
                exit 1
            fi
            args_processed=1
            ;;

        build)
            cmd=$CMD_BUILD
            ;;

        doc)
            cmd=$CMD_DOC
            shift
            get_packages

            do_doc_args $@
            if [[ $? -ne 0 ]]
            then
                exit 1
            fi
            args_processed=1
            ;;

        run)
            cmd=$CMD_RUN
            ;;

        test)
            cmd=$CMD_TEST
            ;;
    esac

    # parse arguments
    if [[ $args_processed -eq 0 ]]
    then
        while [ ! -z "$1" ]
        do
            case $1 in
                -h | --help)        # Display help information and exit
                    display_help
                    ;;

                *)
                    echo "[Error] Argument '$1' provided, but it's currently not supported"
                    exit 1
                    ;;
            esac

            shift
        done
    fi
fi

if [ "$ci" = 1 ]
then
    # Cleanup conflict files
    echo "Cleanup conflict files"
    find . -type f -name "*conflicted copy*" | while read file
    do
        echo "> Cleanup file '$file'"
        rm -f "$file"
    done

    # Cleanup stackdump files
    echo "Cleanup stackdump files"
    find . -type f -name "*stackdump" | while read file
    do
        echo "> Cleanup file '$file'"
        rm -f "$file"
    done

    # Cleanup .DS_Store files
    echo "Cleanup .DS_Store files"
    find . -type f -name ".DS_Store" | while read file
    do
        echo "> Cleanup file '$file'"
        rm -f "$file"
    done

    # Cleanup temporary files
    echo "Cleanup temporary files"
    find target/tmp -type f | while read file
    do
        echo "> Cleanup temporary file '$file'"
        rm -f "$file"
    done
    find target/tmp -type d -mindepth 1 | while read directory
    do
        echo "> Cleanup temporary directory '$directory'"
        rm -rf "$directory"
    done

    # Continuous integration
    echo "Running continuous integration cycle"

    # Read configuration files
    if [ -f $ci_config ]
    then
        echo "> Read CI config file '$ci_config'"
        read_ci_config $ci_config
    else
        if [ -f $default_ci_config ]
        then
            echo "> Read default CI config file '$default_ci_config'"
            read_ci_config $default_ci_config
        else
            echo "> Set default CI config values"
            cycle_delay=60
            error_delay=60
            package_delay=5
            ci_path=$ci_log_path
        fi
    fi

    # If the ci log path doesn't exist, create it
    if [ ! -d $ci_log_path ]
    then
        echo "> Creating CI log path '$ci_log_path'"
        mkdir -p $ci_log_path
    fi

    # Get cycle count
    if [ -f $ci_path/CYCLE_COUNT ]
    then
        echo "> Reading cycle count from '$ci_path/CYCLE_COUNT'"
        i=$(cat $ci_path/CYCLE_COUNT)
    else
        echo "> Set cycle count to '0'"
        i=0
    fi

    while [ 1 = 1 ]
    do
        ((i = i + 1))
        echo "> Starting cycle $i"

        echo "  > Get packages"
        get_packages

        echo "  > Process packages"
        for package in "${!packages[@]}"
        do
            echo "      >- Package '$package' in path '${packages[$package]}'"

            echo "         >- Build package"
            cargo $cargo_params build --package $package > $ci_log_path/$package.build 2>&1
            if [[ $? -ne 0 ]]
            then
                echo "[Error] Build of package '$package' failed"
                echo "--------------------------------------------------------------"
                cargo $cargo_params build --package $package
                echo "--------------------------------------------------------------"
                sleep $error_delay

            else
                echo "         >- Test package"
                cargo $cargo_params test --package $package > $ci_log_path/$package.test 2>&1
                if [[ $? -ne 0 ]]
                then
                    echo "[Error] Test of package '$package' failed"
                    echo "--------------------------------------------------------------"
                    cargo $cargo_params test --package $package
                    echo "--------------------------------------------------------------"
                    sleep $error_delay

                fi
            fi
            echo "      <- Package '$package' processed"
            echo
            sleep $package_delay
        done

        echo "  > Run user specified tests from '$ci_path/USER_SPECIFIED_TESTS'"
        if [ -f $ci_path/USER_SPECIFIED_TESTS ]
        then
            rm -rf $ci_log_path/user_specified_tests.log
            touch $ci_log_path/user_specified_tests.log

            cat $ci_path/USER_SPECIFIED_TESTS | while read cmd
            do
                echo "    > Run command '$cmd'"
                echo ">" >> $ci_log_path/user_specified_tests.log
                echo ">- Run command '$cmd'" >> $ci_log_path/user_specified_tests.log
                echo ">" >> $ci_log_path/user_specified_tests.log

                cargo $cargo_params run $cmd >> $ci_log_path/user_specified_tests.log 2>&1
                if [[ $? -ne 0 ]]
                then
                    echo "[Error] Run failed for '$cmd'"
                    echo "--------------------------------------------------------------"
                    cargo $cargo_params run $cmd
                    echo "--------------------------------------------------------------"
                    sleep $error_delay
                else
                    echo "      >- Command returned success"
                fi
                sleep $package_delay
            done
        else
            echo "    > No user specified tests found"
        fi

        echo "$i" > $ci_path/CYCLE_COUNT
        echo "> End cycle $i"
        echo
        sleep $cycle_delay
    done

    exit 0
fi

# Other functionality
case $cmd in
    $CMD_ADD)
        do_add
        exit $?
        ;;

    $CMD_BUILD)
        echo "[TODO] BUILD command"
        ;;

    $CMD_DOC)
        do_doc
        exit $?
        ;;

    $CMD_RUN)
        echo "[TODO] RUN command"
        ;;

    $CMD_TEST)
        echo "[TODO] TEST command"
        ;;
esac

exit 1
