#
# build.ps1 - Windows build script
#

#
# This script contains some shortcuts to configura, build and test the project. It also,
# when allows the developer to run a continuous integration cycle on the project.
#
# Usage:
#   build.sh <command> [options] [PROJECT_PATH]
#
# Commands:
#   add     Add a new package, binary, test, example, ...
#   build   Build all, a package, a binary, a library, ...
#   ci      Continuous integration
#   doc     Documentation maintenance functions
#   run     Run a binary, an example, ...
#   test    Run tests
#
# Flags:
#   -h, --help  Display help information and exit
#
# Variables:
#   PROJECT_PATH    Project root path
#

#---
# Continuous integration cycle
#---
# The continuous integration cycle performs the following tasks.
#
#   > At program start
#       - Remove conflict files (created by Dropbox)
#       - Remove stackdump files (created by PowerShell, when SSH connection fails)
#       - Remove .DS_Store files (created by MacOSX)
#       - Cleanup temporary files (in target/tmp)
#       - Read the configuration file (.ci.<hostname>.toml in the project root)
#       - If the CI log path doesn't exist, create it
#       - Read or set the cycle count (ci_path/CYCLE_COUNT)
#
#   > CI cycle (repeats indefinitely)
#       - Get packages
#       - For each package found
#           - Run CARGO BUILD
#               - If an error occurs, run and display build information
#           - Run CARGO TEST
#               - If an error occurs, run tests and display results
#       - [TODO] Run integration tests
#       - Run user specified tests (ci_path/USER_SPECIFIED_TESTS)
#       - Increase and save cycle count in ci_path/CYCLE_COUNT
#---

$BUILD_SCRIPT_VERSION = "0.0.1"

#
# Paths
#
$global:script_path = "."

$global:ci_log_path = "./target/ci"

$global:tools_path = "./tools"
$global:tools_build_path = "$tools_path/build"

#
# Constants
#

# Main executable package
$MAIN_EXE_PKG = "tundra-wolf"

# Plugin types
$PI_ANALYZER = 1
$PI_GENERATOR = 2
$PI_REFACTORER = 3

# Test types
$T_INTEGRATION_TEST = 1
$T_BENCHMARK = 2
$T_UNIT_TEST = 3

#
# Includes
#
. $tools_build_path/functions.ps1
. $tools_build_path/add_command.ps1
. $tools_build_path/doc_command.ps1

#---
# Get all packages in the project.
#---
$global:packages = @{}

$PT_BINARY = 1
$PT_LIBRARY = 2
$PT_ANALYZER = 3
$PT_GENERATOR = 4
$PT_REFACTORER = 5

$global:package_types = @{}

function Get-Packages {
    # Get library packages
    $paths = Get-ChildItem -Path . -Depth 1 -Filter tw-* -Recurse -Directory | Select-Object Name, FullName

    ForEach ($path in $paths) {
        $name = $path.Name
        $path = $path.FullName
        $global:packages[$name] = $path
        $global:package_types[$name] = $PT_LIBRARY
    }

    # Get analyzer plugin packages
    $paths = Get-ChildItem -Path . -Depth 2 -Filter twa-* -Recurse -Directory | Select-Object Name, FullName

    ForEach ($path in $paths) {
        $name = $path.Name
        $path = $path.FullName
        $global:packages[$name] = $path
        $global:package_types[$name] = $PT_ANALYZER
    }

    # Get generator plugin packages
    $paths = Get-ChildItem -Path . -Depth 2 -Filter twg-* -Recurse -Directory | Select-Object Name, FullName

    ForEach ($path in $paths) {
        $name = $path.Name
        $path = $path.FullName
        $global:packages[$name] = $path
        $global:package_types[$name] = $PT_ANALYZER
    }

    # Get refactorer plugin packages
    $paths = Get-ChildItem -Path . -Depth 2 -Filter twr-* -Recurse -Directory | Select-Object Name, FullName

    ForEach ($path in $paths) {
        $name = $path.Name
        $path = $path.FullName
        $global:packages[$name] = $path
        $global:package_types[$name] = $PT_ANALYZER
    }

    # Get binary packages
    $paths = Get-ChildItem -Path . -Depth 0 -Filter tundra-wolf* -Recurse -Directory | Select-Object Name, FullName

    ForEach ($path in $paths) {
        $name = $path.Name
        $path = $path.FullName
        $global:packages[$name] = $path
        $global:package_types[$name] = $PT_ANALYZER
    }
}

#---
# Read CI Configuration File
#---
$global:default_ci_config="./.ci.toml"

$hostname = @(hostname)
$global:ci_config = "./.ci.$hostname.toml"

$global:cycle_delay = 60
$global:error_delay = 60
$global:package_delay = 5
$global:ci_path = "./target/ci"

$global:cargo_params = @()

function Read-CI-Config {
    param (
        [Parameter(Mandatory)]
        [string]$config_file
    )

    $tmp = Select-String -Path $config_file -Pattern ^cycle-delay
    if ($tmp) {
        $tmp = $tmp -split "=", -1
        $global:cycle_delay = $($tmp[1]).Trim().Replace('"', '')
    }

    $tmp = Select-String -Path $config_file -Pattern ^error-delay
    if ($tmp) {
        $tmp = $tmp -split "=", -1
        $global:error_delay = $($tmp[1]).Trim().Replace('"', '')
    }

    $tmp = Select-String -Path $config_file -Pattern ^package-delay
    if ($tmp) {
        $tmp = $tmp -split "=", -1
        $global:package_delay = $($tmp[1]).Trim().Replace('"', '')
    }

    $tmp = Select-String -Path $config_file -Pattern ^ci-path
    if ($tmp) {
        $tmp = $tmp -split "=", -1
        $global:ci_path = $($tmp[1]).Trim().Replace('"', '')
        $global:ci_log_path = $ci_path
    }

    $global:cargo_params = "--config", "build.target-dir='$ci_path\..'"

    return 0
}

#---
# Main program
#---

$global:arguments = $args

$global:ci = $false

$NO_COMMAND = 0

$global:cmd = $NO_COMMAND

$CMD_ADD = 1
$CMD_BUILD = 2
$CMD_DOC = 3
$CMD_RUN = 4
$CMD_TEST = 5

if ($arguments.Length -eq 0) {
    $ci = $true
} else {
    $shift = $true
    $args_processed = $false

    # First argument is supposed to be a command or -h/--help
    $arg = $($arguments[0])

    switch ($arg) {
        ci {
            $ci = $true
        }
        add {
            $cmd = $CMD_ADD
            $__, $args = $arguments
            $status = Do-Add-Args @args
            if ($status -ne 0) {
                Exit 1
            }
            $args_processed = $true
        }
        build {
            $cmd = $CMD_BUILD
        }
        doc {
            $cmd = $CMD_DOC
            $__, $args = $arguments
            $status = Do-Doc-Args @args
            if ($status -ne 0) {
                Exit 1
            }
            $args_processed = $true
        }
        run {
            $cmd = $CMD_RUN
        }
        test {
            $cmd = $CMD_TEST
        }
        Default {
            $shift = $false
        }
    }

    # Skip processed argument
    if ($shift) {
        $__, $arguments = $arguments
    }

    if (-Not $args_processed) {
        foreach ($arg in $arguments) {
            switch ($arg) {
                "-h" {
                    Display-Help
                }
                "--help" {
                    Display-Help
                }
            }
        }
    }
}

# If no other command is given, run CI
if ($cmd -eq $NO_COMMAND) {
    $ci = $true
}

# Continuous integration
if ($ci) {
    # Cleanup conflict files
    Write-Host ("Cleanup conflict files")
    $files = Get-ChildItem -Path . -Filter "*conflict*copy*" -Recurse | Select-Object FullName

    ForEach ($file in $files) {
        $file = $file.FullName
        Write-Host ("> Cleanup file '$file'")
        Remove-Item -Path $file
    }

    # Cleanup stackdump files
    Write-Host ("Cleanup stackdump files")
    $files = Get-ChildItem -Path . -Filter "*stackdump" -Recurse | Select-Object FullName

    ForEach ($file in $files) {
        $file = $file.FullName
        Write-Host ("> Cleanup file '$file'")
        Remove-Item -Path $file
    }

    # Cleanup .DS_Store files
    Write-Host ("Cleanup .DS_Store files")
    $files = Get-ChildItem -Path . -Filter ".DS_Store" -Recurse | Select-Object FullName

    ForEach ($file in $files) {
        $file = $file.FullName
        Write-Host ("> Cleanup file '$file'")
        Remove-Item -Path $file
    }

    # Cleanup temporary files
    Write-Host ("Cleanup temporary files")
    $files = Get-ChildItem -Path ./target/tmp -Recurse | Select-Object FullName

    ForEach ($file in $files) {
        $file = $file.FullName
        Write-Host ("> Cleanup temporary file '$file'")
        Remove-Item -Path $file
    }

    # Continuous integration
    Write-Host("Running continuous integration cycle")

    # Read configuration file
    if (Test-Path $ci_config -PathType Leaf) {
        Write-Host ("> Read CI config file '$ci_config'")
        $status = Read-CI-Config ($ci_config)
        if ($status -ne 0) {
            Write-Host ("[Error] Read CI configuration from '$ci_config' failed")
            Exit 1
        }
    } else {
        if (Test-Path $default_ci_config -PathType Leaf) {
            Write-Host ("> Read default CI config file '$default_ci_config'")
            $status = Read-CI-Config ($default_ci_config)
            if ($status -ne 0) {
                Write-Host ("[Error] Read CI configuration from '$default_ci_config' failed")
                Exit 1
            }
        } else {
            Write-Host ("> Set default CI config values")
            $cycle_delay=60
            $error_delay=60
            $package_delay=5
            $ci_path="$ci_log_path"
        }
    }

    # If the log path doesn't exist, create it
    if (-Not (Test-Path $ci_path)) {
        Write-Host ("> Create path '$ci_path'")
        New-Item -Path "$ci_path/.cd" -Force *> $null
    }
    if (-Not (Test-Path $ci_log_path)) {
        Write-Host ("> Create log path '$ci_log_path'")
        New-Item -Path "$ci_log_path/.cd" -Force *> $null
    }

    # Get cycle count
    $i = 0
    if (Test-Path $ci_path/CYCLE_COUNT -PathType Leaf) {
        Write-Host ("> Reading cycle count from '$ci_path/CYCLE_COUNT'")
        $i = Get-Content $ci_path/CYCLE_COUNT -Raw
        $i = $i.Trim()/1
    } else {
        Write-Host ("> Set cycle count to '0'")
        $i = 0
    }

    while(1 -eq 1) {
        $i = $i + 1
        Write-Host("> Starting cycle $i")

        Write-Host("  > Get packages")
        Get-Packages

        Write-Host("  > Process packages")
        ForEach ($package in $packages.keys) {
            Write-Host ("    >- Processing package '$package' in '$($packages.$package)'")

            Write-Host ("       >- Build package")
            cargo $cargo_params build --package $package *> $ci_log_path/$package.build
            if ($LASTEXITCODE -ne 0) {
                Write-Host ("[Error] Build of package '$package' failed")
                Write-Host ("-------------------------------------------------------")
                cargo $cargo_params build --package $package
                Write-Host ("-------------------------------------------------------")
                Start-Sleep -s $error_delay
            } else {
                Write-Host ("       >- Test package")
                cargo $cargo_params test --package $package *> $ci_log_path/$package.test
                if ($LASTEXITCODE -ne 0) {
                    Write-Host ("[Error] Test of package '$package' failed")
                    Write-Host ("-------------------------------------------------------")
                    cargo $cargo_params test --package $package
                    Write-Host ("-------------------------------------------------------")
                    Start-Sleep -s $error_delay
                }
            }

            Write-Host ("    <- Package '$package' processed")
            Write-Host ("")
            Start-Sleep -s $package_delay
        }

        Write-Host ("  > Run user specific tests from '$ci_path/USER_SPECIFIED_TESTS'")
        if (Test-Path $ci_path/USER_SPECIFIED_TESTS -PathType Leaf) {
            if (Test-Path $ci_log_path/user_specified_tests.log -PathType Leaf) {
                Remove-Item -Path $ci_log_path/user_specified_tests.log -Force
            }

            ForEach($cmd in Get-Content $ci_path/USER_SPECIFIED_TESTS) {
                Write-Host ("    > Run command '$cmd'")
                ">" >> $ci_log_path/user_specified_tests.log
                ">- Run command '$cmd'" >> $ci_log_path/user_specified_tests.log
                ">" >> $ci_log_path/user_specified_tests.log

                $params = $cmd.Split(" ")

                cargo $cargo_params run $params *>> $ci_log_path/user_specified_tests.log
                if ($LASTEXITCODE -ne 0) {
                    Write-Host ("[Error] Run failed for '$cmd'")
                    Write-Host ("-------------------------------------------------------")
                    cargo $cargo_params run $params
                    Write-Host ("-------------------------------------------------------")
                    Start-Sleep -s $error_delay
                } else {
                    Write-Host ("      >- Command returned success")
                }
                Start-Sleep -s $package_delay
            }
        } else {
            Write-Host ("    > No user specified tests found")
        }

        $i | Out-File $ci_path/CYCLE_COUNT
        Write-Host ("> End cycle $i")
        Write-Host ("")
        Start-Sleep -s $cycle_delay
    }
    Exit 0
}

switch ($cmd) {
    $CMD_ADD {
        Do-Add
        Exit $LASTEXITCODE
    }
    $CMD_BUILD {
        Write-Host ("[Error] Command 'build' is not implemented yet")
        Exit 1
    }
    $CMD_DOC {
        Do-Doc
        Exit $LASTEXITCODE
    }
    $CMD_RUN {
        Write-Host ("[Error] Command 'run' is not implemented yet")
        Exit 1
    }
    $CMD_TEST {
        Write-Host ("[Error] Command 'test' is not implemented yet")
        Exit 1
    }
    default {
        Write-Host ("[Error] There is not command with number '$cmd'")
        Exit 1
    }
}

Exit 0
