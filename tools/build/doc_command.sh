#
# build/doc_command.sh - UNIX build script (doc command)
#

#
# The doc command is used to work with documentation. It is used to generate
# documentation from the source code, but also to update certain parameters and to
# generate the documentation in HTML, PDF and other formats.
#
# Usage:
#   build.sh doc <subcommand> [options] <pacakge>... [PROJECT_PATH]
#
# Subcommands:
#   add             Add a new document
#   generate-api    Generate API documentation based on source code (also gen-api, gapi)
#   update          Update parameter in all documentation
#
# For 'add'
#
# For 'generate-api'
#   Flags:
#       -f, --force     Force the generate API action even if documentation is up-to-date
#
#   Options:
#       -f, --format <file>
#               Use settings in <file> to format the resulting documents
#       -h, --html <html-option>
#               Options for generating HTML
#           by-chapter, single-page (default)
#           interactive (default), static
#           text-only
#       -H, --highest-level <level>
#               Highest level of document part to generate
#           chapter (default), document, part, section, subsection
#       -p, --page-setup <setup-option>
#               Options for generating page layouts (for pdf, troff)
#           3-color, black-white, full-color
#           a3, a4 (default), a5, us-letter
#           double-sided (default), single-sided
#       -t, --type <type>
#               Type of documentation to generate (multiple are allowed)
#           asciidoc (default), html, markdown, pdf, troff
#
# For 'update'
#
# Variables:
#   PROJECT_PATH    Project root path, or some path in the project root
#

#---
# Parse ADD command arguments
#---

# document subcommand
readonly DOC_ADD=1
readonly DOC_GENERATE_API=2
readonly DOC_UPDATE=3

doc_cmd=

# format file
doc_format_file=

# highest level
readonly DOC_HL_CHAPTER=1
readonly DOC_HL_DOCUMENT=2
readonly DOC_HL_PART=3
readonly DOC_HL_SECTION=4
readonly DOC_HL_SUBSECTION=5

doc_highest_level=$DOC_HL_CHAPTER

# document type
readonly DOC_T_ASCIIDOC=1
readonly DOC_T_HTML=2
readonly DOC_T_MARKDOWN=3
readonly DOC_T_PDF=4
readonly DOC_T_TROFF=5

doc_type=$DOC_T_ASCIIDOC

# html options
readonly DOC_HTML_SINGLE_PAGE=1
readonly DOC_HTML_BY_CHAPTER=2

readonly DOC_HTML_INTERACTIVE=3
readonly DOC_HTML_STATIC=4

readonly DOC_HTML_TEXT_ONLY=5

typeset -A doc_html_options
doc_html_package[0]=$DOC_HTML_SINGLE_PAGE
doc_html_package[1]=$DOC_HTML_INTERACTIVE

# document size
readonly DOC_PS_A3=1
readonly DOC_PS_A4=2
readonly DOC_PS_A5=3
readonly DOC_PS_US_LETTER=4

doc_page_size=$DOC_PS_A4

# double sided?
doc_double_sided=1

# document colors
readonly DOC_C_3_COLOR=1
readonly DOC_C_BLACK_WHITE=2
readonly DOC_C_FULL_COLOR=3

doc_color=$DOC_C_3_COLOR

# other variables
typeset -A doc_packages

# force document action, even if up-to-date
doc_force=0

function do_doc_args
{
    if [ -z $1 ]
    then
        echo "[Error] 'doc' command without arguments"
        return 1
    fi

    # Subcommand
    case $1 in
        -h | --help)
            display_help
            ;;

        add)
            doc_cmd=$DOC_ADD
            shift
            ;;

        generate-api | gen-api | gapi)
            doc_cmd=$DOC_GENERATE_API
            shift
            ;;

        update | upd)
            doc_cmd=$DOC_UPDATE
            shift
            ;;
    esac

    # Other arguments
    typeset n=0

    while [ ! -z $1 ]
    do
        case $1 in
            -f | --format)
                shift
                if [ -f "$1" ]
                then
                    doc_format_file=$1
                else
                    echo "[Error] Format file '$1' doesn't exist"
                    return 1
                fi
                ;;

            -h | --html)
                shift
                case $1 in
                    by-chapter)
                        doc_html_options[0]=$DOC_HTML_BY_CHAPTER
                        ;;

                    interactive)
                        doc_html_options[1]=$DOC_HTML_INTERACTIVE
                        ;;

                    single-page)
                        doc_html_options[0]=$DOC_HTML_SINGLE_PAGE
                        ;;

                    static)
                        doc_html_options[1]=$DOC_HTML_STATIC
                        ;;

                    text-only)
                        doc_html_options[2]=$DOC_HTML_TEXT_OMLY
                        ;;

                    *)
                        echo "[Error] Invalid HTML option '$1'"
                        return 1
                        ;;
                esac
                ;;

            -H | --highest-level)
                shift
                case $1 in
                    chapter)
                        doc_highest_level=$DOC_HL_CHAPTER
                        ;;

                    document)
                        doc_highest_level=$DOC_HL_DOCUMENT
                        ;;

                    part)
                        doc_highest_level=$DOC_HL_PART
                        ;;

                    section)
                        doc_highest_level=$DOC_HL_SECTION
                        ;;

                    subsection)
                        doc_highest_level=$DOC_HL_SUBSECTION
                        ;;

                    *)
                        echo "[Error] Invalid highest level '$1'"
                        return 1
                        ;;
                esac
                ;;

            -p | --page-setup)
                shift
                case $1 in
                    3-color)
                        doc_color=$DOC_C_3_COLOR
                        ;;

                    a3)
                        doc_page_size=$DOC_PS_A3
                        ;;

                    a4)
                        doc_page_size=$DOC_PS_A4
                        ;;

                    a5)
                        doc_page_size=$DOC_PS_A5
                        ;;

                    black-white)
                        doc_color=$DOC_C_BLACK_WHITE
                        ;;

                    double_sided)
                        doc_double_sided=1
                        ;;

                    full_color)
                        doc_color=$DOC_C_FULL_COLOR
                        ;;

                    single-sided)
                        doc_double_sided=0
                        ;;

                    us-letter)
                        doc_page_size=$DOC_PS_US_LETTER
                        ;;

                    *)
                        echo "[Error] Invalid page setup option '$1'"
                        return 1
                        ;;
                esac
                ;;

            -t | --type)
                shift
                case $1 in
                    asciidoc)
                        doc_type=$DOC_T_ASCIIDOC
                        ;;

                    html)
                        doc_type=$DOC_T_HTML
                        ;;

                    markdown)
                        doc_type=$DOC_T_MARKDOWN
                        ;;

                    pdf)
                        doc_type=$DOC_T_PDF
                        ;;

                    troff)
                        doc_type=$DOC_T_TROFF
                        ;;

                    *)
                        echo "[Error] Invalid document type '$1'"
                        return 1
                        ;;
                esac
                ;;

            *)
                if [ ! -z "${packages[$1]}" ]
                then
                    doc_packages[$n]=$1
                    ((n = n + 1))
                else
                    echo "[Error] Invalid package '$1' provided"
                    return 1
                fi
        esac

        shift
    done

    # Check arguments
    if [ -z $doc_cmd ]
    then
        echo "[Error] The second argument was not a 'subcommand"
        return 1
    elif [ $doc_cmd -eq $DOC_ADD ]
    then
        # DOC ADD check arguments
        echo "[TODO] 'doc add' is not implemented yet"
        return 1
    elif [ $doc_cmd -eq $DOC_GENERATE_API ]
    then
        do_doc_generate_api_args
        return $?
    elif [ $doc_cmd -eq $DOC_UPDATE ]
    then
        # DOC UPDATE check arguments
        echo "[TODO] 'doc update' is not implemented yet"
        return 1
    else
        echo "[Error] Invalid subcommand '$doc_cmd' (Should not be reachable)"
        return 1
    fi
}

#---
# Check DOC GENERATE-API arguments
#---
function do_doc_generate_api_args
{
    # Read format file
    if [ ! -z $doc_format_file ]
    then
        do_doc_read_format_file $doc_format_file
        if [ $? -ne 0 ]
        then
            echo "[Error] Invalid format file '$doc_format_file'"
            return 1
        fi
    fi

    # Check arguments
    if [[ $doc_type -eq $DOC_T_ASCIIDOC ]]
    then
        # Only highest level is relevant, all levels are supported
        return 0
    elif [[ $doc_type -eq $DOC_T_MARKDOWN ]]
    then
        # Check highest level, 'part' is not supported
        if [ $doc_highest_level -eq $DOC_HL_PART ]
        then
            echo "[Error] Highest level 'part' is not supported by type 'Markdown'"
            return 1
        fi

        return 0
    elif [[ $doc_type -eq $DOC_T_HTML ]]
    then
        # Check highest level, 'part' is not supported
        if [ $doc_highest_level -eq $DOC_HL_PART ]
        then
            echo "[Error] Highest level 'part' is not supported by type 'Markdown'"
            return 1
        fi

        # All html options are supported
        return 0
    elif [[ $doc_type -eq $DOC_T_PDF || $doc_type -eq $DOC_T_TROFF ]]
    then
        # Only highest level and page setup is supported, all values are supported
        return 0
    else
        echo "[Error] Invalid document type '$doc_type'"
        return 1
    fi
}

#---
# Read and check format file
#---
function do_doc_read_format_file
{
    if [ -z "$1" ]
    then
        echo "[Error] The format file is mandatory"
        return 1
    fi

    typeset format_file=$1

    echo "[TODO] Read format file '$format_file'"

    return 0
}

#---
# Run ADD command
#---
function do_doc
{
    for package in "${doc_packages[@]}"
    do
        echo "Package: '$package'"
    done

    return 1
}
