#
# build/add_command.sh - UNIX build script (add command)
#

#
# The add command to the build script is used to add new items to the project. The
# following items can be added using the script.
#
# Usage:
#   build.sh add <subcommand> [options] [PROJECT_PATH]
#
# Subcommands:
#   library     A new library package
#   plugin      A new plugin library (the plugin type defaults to refactorer).
#
#   binary      A new binary crate, if a package is given, it is added to that package,
#               otherwise a new package is created in the project root.
#   module      A new module, both a package and parent module are mandatory.
#
#   benchmark   A new benchmark, if no package is given, to the $MAIN_EXE_PKG package.
#   example     A new example, if no package is given, to the $MAIN_EXE_PKG package.
#   test        A new test. For unit tests, both the package and module are mandatory.
#               For integration tests, the pacakge is mandatory.
#
#   document    A new document in either AsciiDoc or Markdown format
#
# For 'plugin'
#   Flags:
#       -A, --analyzer      An analyzer plugin
#       -f, --force         Force the add action
#       -G, --generator     A generator plugin
#       -R, --refactorer    A refactorer plugin
#
#   Options:
#       -P, --plugin <type>     A plugin of <type>
#           analyzer, generator, refactorer (default)
#

#---
# Parse ADD command arguments
#---

# add subcommand
readonly ADD_LIBRARY=1
readonly ADD_BINARY=2
readonly ADD_PLUGIN=3
readonly ADD_MODULE=4
readonly ADD_EXAMPLE=5
readonly ADD_TEST=6

add_cmd=

# test
add_test=

# module
add_module=
add_package=
add_parent_module=

# binary
add_binary=
add_binary_package=

# plugin
add_plugin=
add_plugin_group=
add_plugin_package=
add_plugin_path=
add_plugin_type=0

# library
add_library=
add_library_group=
add_library_package=
add_library_path=

# force creation, even if the object exists
add_force=0

function do_add_args
{
    if [ -z $1 ]
    then
        echo "[Error] 'add' command without arguments"
        return 1
    fi

    # Subccommand
    case $1 in
        -h | --help)
            display_help
            ;;

        binary)
            add_cmd=$ADD_BINARY
            shift
            ;;

        example)
            add_cmd=$ADD_EXAMPLE
            shift
            ;;

        library)
            add_cmd=$ADD_LIBRARY
            shift
            ;;

        module)
            add_cmd=$ADD_MODULE
            shift
            ;;

        plugin)
            add_cmd=$ADD_PLUGIN
            shift
            ;;

        test)
            add_cmd=$ADD_TEST
            shift
            ;;
    esac

    # Other parameters
    while [ ! -z $1 ]
    do
        case $1 in
            -A | --analyzer)
                add_plugin_type=$PI_ANALYZER
                ;;

            -f | --force)
                add_force=1
                ;;

            -G | --generator)
                add_plugin_type=$PI_GENERATOR
                ;;

            -P | --plugin)
                if [ -z $2 ]
                then
                    echo "[Error] Plugin type parameter without value"
                    return 1
                fi
                case $2 in
                    analyzer)
                        add_plugin_type=$PI_ANALYZER
                        ;;

                    generator)
                        add_plugin_type=$PI_GENERATOR
                        ;;

                    refactorer)
                        add_plugin_type=$PI_REFACTORER
                        ;;

                    *)
                        echo "[Error] Invalid plugin type '$2'"
                        return 1
                        ;;
                esac
                shift
                ;;

            -R | --refactorer)
                add_plugin_type=$PI_REFACTORER
                ;;

            *)
                add_name=$1
                ;;
        esac

        shift
    done


    # Check arguments
    if [ -z $add_cmd ]
    then
        echo "[Error] The second argument was not a 'subcommand'"
        return 1
    elif [ $add_cmd -eq $ADD_BINARY ]
    then
        # ADD BINARY check arguments
        echo "[TODO] 'add binary' is not implemented yet"
        return 1
    elif [ $add_cmd -eq $ADD_EXAMPLE ]
    then
        # ADD EXAMPLE check arguments
        echo "[TODO] 'add example' is not implemented yet"
        return 1
    elif [ $add_cmd -eq $ADD_LIBRARY ]
    then
        # ADD LIBRARY check arguments
        do_add_library_args
        return $?
    elif [ $add_cmd -eq $ADD_MODULE ]
    then
        # ADD MODULE check arguments
        echo "[TODO] 'add module' is not implemented yet"
        return 1
    elif [ $add_cmd -eq $ADD_PLUGIN ]
    then
        # ADD PLUGIN check arguments
        do_add_plugin_args
        return $?
    elif [ $add_cmd -eq $ADD_TEST ]
    then
        # ADD TEST check arguments
        echo "[TODO] 'add test' is not implemented yet"
        return 1
    else
        echo "[Error] Invalid subcommand '$add_cmd' (Should not be reachable)"
        return 1
    fi
}

#---
# Check ADD LIBRARY arguments
#---
function do_add_library_args
{
    add_library=$add_name

    add_library_group="tundra-wolf/libs"
    add_library_package="tw-$add_library"
    add_library_path="./libs"

    # Check if the library already exists
    check_package $add_library_path $add_library_package
    if [[ $? -ne 0 ]]
    then
        if [[ $add_force -eq 0 ]]
        then
            echo "[Error] The library with name '$add_library_package' already exists"
            return 1
        fi
    fi

    return 0
}

#---
# Check ADD PLUGIN arguments
#---
function do_add_plugin_args
{
    add_plugin=$add_name

    # If no plugin type, default to 'refactorer'
    if [ $add_plugin_type -eq 0 ]
    then
        add_plugin_type=$PI_REFACTORER
    fi

    if [ $add_plugin_type -eq $PI_ANALYZER ]
    then
        add_plugin_group="tundra-wolf/analyzers"
        add_plugin_package="twa-$add_plugin"
        add_plugin_path="./plugins/analyzers"
    elif [ $add_plugin_type -eq $PI_GENERATOR ]
    then
        add_plugin_group="tundra-wolf/generators"
        add_plugin_package="twg-$add_plugin"
        add_plugin_path="./plugins/generators"
    elif [ $add_plugin_type -eq $PI_REFACTORER ]
    then
        add_plugin_group="tundra-wolf/refactorers"
        add_plugin_package="twr-$add_plugin"
        add_plugin_path="./plugins/refactorers"
    else
        echo "[Error] The plugin type '$add_plugin_type' is invalid"
        return 1
    fi

    # Check if the plugin already exists
    check_package $add_plugin_path $add_plugin_package
    if [[ $? -ne 0 ]]
    then
        if [[ $add_force -eq 0 ]]
        then
            echo "[Error] The plugin with name '$add_plugin_package' already exists"
            return 1
        fi
    fi

    return 0
}

#---
# Run ADD command
#---
function do_add
{
    if [ $add_cmd -eq $ADD_BINARY ]
    then
        echo "[TODO] Implement add binary command"
    elif [ $add_cmd -eq $ADD_EXAMPLE ]
    then
        echo "[TODO] Implement add example command"
    elif [ $add_cmd -eq $ADD_LIBRARY ]
    then
        do_add_library
        if [[ $? -ne 0 ]]
        then
            echo "[Error] Create of library '$add_library' in '$add_library_path' failed"
            return 1
        fi
    elif [ $add_cmd -eq $ADD_MODULE ]
    then
        echo "[TODO] Implement add module command"
    elif [ $add_cmd = $ADD_PLUGIN ]
    then
        do_add_plugin
        if [[ $? -ne 0 ]]
        then
            echo "[Error] Create of plugin '$add_plugin' in '$add_plugin_path' failed"
            return 1
        fi
    elif [ $add_cmd -eq $ADD_TEST ]
    then
            echo "[TODO] Implement add test command"
    fi

    return 0
}

#---
# Run ADD LIBRARY command
#
# Usage:
#   do_add_lirary
#
# Returns:
#   0   Successful creating of the library project
#   1   An error occured creating the library project
#---
function do_add_library
{
    echo "Creating library '$add_library_package' in '$add_library_path'"

    do_add_package $add_library_group $add_library_package $add_library_path
    if [[ $? -ne 0 ]]
    then
        echo "[Error] An error occured creating the library '$add_library_package'"
        return 1
    fi

    return 0
}

#---
# Run ADD PLUGIN command
#
# Usage:
#   do_add_plugin
#
# Returns:
#   0   Successful creating of the library project
#   1   An error occured creating the library project
#---
function do_add_plugin
{
    echo "Creating plugin '$add_plugin_package' in '$add_plugin_path'"

    do_add_package $add_plugin_group $add_plugin_package $add_plugin_path
    if [[ $? -ne 0 ]]
    then
        echo "[Error] An error occured creating the plugin '$add_plugin_package'"
        return 1
    fi

    return 0
}

#---
# Run ADD <package> command
#   where <package> = LIBRARY
#                     PLUGIN
#
# Usage:
#   do_add_package <group> <package> <path>
#
# Returns:
#   0   Successfully created the package
#   1   An error occured creating the package
#---
function do_add_package
{
    if [ $# -ne 3 ]
    then
        echo "[Error] The function 'do_add_package' requires 3 arguments: group, package, path"
        return 1
    fi

    typeset group="$1"
    typeset package="$2"
    typeset path="$3"

    echo "Creating plugin '$package' in '$path'"

    # Create Cargo package in tmp
    echo "> Create Cargo package in '$tmp_path'"
    rm -rf "$tmp_path/$package" $tmp_path/Cargo.*

    cd $tmp_path

    cargo $cargo_params new --lib $package > $tmp_path/${package}.log 2>&1
    if [[ $? -ne 0 ]]
    then
        echo "[Error] Create Cargo package '$tmp_path/$package' failed"
        return 1
    fi

    # Init git repository
    echo "> Init GIT repository for '$package'"
    cd $tmp_path/$package
    git init >> $tmp_path/${package}.log 2>&1

    # Add initial files
    #   COPYING
    #   COPYING_FDL
    #   .gitignore
    #   README.md
    echo "> Add base files to '$package'"
    cp $script_path/COPYING* .
    echo "**/.DS_Store" > .gitignore
    echo "" > README.md

    echo "> Add base files to GIT repository"
    git add COPYING* .gitignore README.md Cargo.toml src/ \
        >> $tmp_path/${package}.log 2>&1
    if [[ $? -ne 0 ]]
    then
        echo "[Error] Add files to git failed for '$package'"

        rm -rf "$tmp_path/$package"
        return 1
    fi

    # Commit files to new project
    echo "> Commit base files to GIT repository"
    git commit -m "initial commmit" >> $tmp_path/${package}.log 2>&1
    if [[ $? -ne 0 ]]
    then
        echo "[Error] Commit files to git failed for '$package'"

        rm -rf "$tmp_path/$package"
        return 1
    fi

    # Create remote repository
    echo "> Add remote repository as '$group/$package'"
    gitlab_create_project $group $package >> $tmp_path/${package}.log 2>&1
    if [[ $? -ne 0 ]]
    then
        echo "[Error] Create remote project failed for '$package'"

        rm -rf "$tmp_path/$package"
        return 1
    fi

    # Remove plugin directory from tmp
    echo "> Remove the temporary plugin directory"
    cd $script_path
    rm -rf $tmp_path/$package >> $tmp_path/${package}.log 2>&1

    # Submodule add to the project root
    echo "> Add submodule '$package' to project root"
    cd $path
    git submodule add "git@gitlab.com:$group/$package" >> $tmp_path/${package}.log 2>&1
    if [[ $? -ne 0 ]]
    then
        echo "[Error] Submodule add for '$package' failed"
        return 1
    fi

    # Add package to the members in project root Cargo.toml
    echo "> Add member '$path/$package' to 'Cargo.toml'"
    cargo_add_to_workspace $path $package
    if [[ $? -ne 0 ]]
    then
        echo "[Error] Adding '$path/$package' to workspace failed"
        return 1
    fi

    return 0
}
