#
# build/add_command.ps1 - Windows build script (add command)
#

#
# The add command to the build script is used to add new items to the project. The
# following items can be added using the script.
#
# Usage:
#   build.sh add <subcommand> [options] [PROJECT_PATH]
#
# Subcommands:
#   library     A new library package
#   plugin      A new plugin library (the plugin type defaults to refactorer).
#
#   binary      A new binary crate, if a package is given, it is added to that package,
#               otherwise a new package is created in the project root.
#   module      A new module, both a package and parent module are mandatory.
#
#   benchmark   A new benchmark, if no package is given, to the $MAIN_EXE_PKG package.
#   example     A new example, if no package is given, to the $MAIN_EXE_PKG package.
#   test        A new test. For unit tests, both the package and module are mandatory.
#               For integration tests, the pacakge is mandatory.
#
#   document    A new document in either AsciiDoc or Markdown format
#
# For 'plugin'
#   Flags:
#       -A, --analyzer      An analyzer plugin
#       -f, --force         Force the add action
#       -G, --generator     A generator plugin
#       -R, --refactorer    A refactorer plugin
#
#   Options:
#       -P, --plugin <type>     A plugin of <type>
#           analyzer, generator, refactorer (default)
#

#---
# Parse ADD command arguments
#---
function Do-Add-Args
{
    Write-Host ("[TODO] Implement add command arguments")
    
    foreach ($arg in $args) {
        Write-Host ("Arg: '$arg'")
    }

    return 1
}

#---
# Run ADD command
#---
function Do-Add
{
    Write-Host ("[TODO] Implement add command")
    
    return 1
}
