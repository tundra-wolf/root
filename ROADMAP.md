# ROADMAP for the Tundra Wolf project

## Overview

The project has 4 phases described as follows. The version numbers indicate the phases.

* < 0.1 - implement conversion and refactoring of a simple ASP.NET project.
* < 0.9 - generalize analysis, generation and refactoring.
* < 1.0 - build graphical and text based UI and optimize/extend plugins.
* 1.0 - production ready
    * new release versions provide bug fixes.
    * new minor versions mainly provide other analyzers, generators and refactorers.
    * new major versions provide new functionality or incompatible optimizations to existing functionality.

## 1.0 Source/Target Support

The focus for version 1.0 is the following:

* Analyse source projects using these languages/technologies.
    * C
    * C++
    * C# / Visual Basic / .NET
    * Java
    * Javascript/WebAssembly
    * NoSQL databases (MongoDB, Neo4J)
    * PowerBuilder
    * Relational databases (Oracle, MariaDB, PostgreSQL, Sybase, MS SQL Server)
    * Rust
    * Scripting: DOS shell, Perl, PowerShell, Python, R, UNIX shell
    * Web (CSS, HTML, Javascript, PHP and numerous web frameworks)
* Generate target projects uisng these languages/technologies.
    * C
    * Korn/Bash shell
    * NoSQL databases
    * Rust
    * Web (CSS, HTML, Javascript, WebAssembly)

## Pre 0.1

### Single file C -> Rust (<= 0.0.7)

* Implement tw-source (0.0.2) - Source Manager, for ASCII and UTF-8 sources

* Implement twa-c (0.0.5) - Basic, single file program C support
    * Implement twa-cpp (0.0.3) - Complete C support
    * Implement twa-projects (0.0.4) - GNU Makefile support

* Implement twa-rust (0.0.6) - Direct translation of C sources

* Testing / Fixes / Refactoring (0.0.7)

### Webload project (<= 0.0.18)

* Implement twa-projects (0.0.8) - For Visual Studio projects

* Implement for the webload project
    * twa-vb (0.0.13)
    * twa-html (0.0.9)
    * twa-css (0.0.10)
    * twa-javascript (0.0.12)
    * twa-sql (0.0.11)

* Implement tw-semrep (0.0.14)

* Implement for the webload project
    * twg-rust (0.0.15)
    * twg-html (0.0.16)
    * twg-documentdb (0.0.17)

* Testing / Fixes / Refactoring (0.0.18)

### C++ -> Rust (<= 0.0.31)

* Implement twa-projects (0.0.19) - CMake support

* Implement twa-cxx (0.0.25) - Small C++ projects
    * twa-cpp (0.0.20) - Complete C++ support
    * twa-cxx (0.0.21) - Basic parsing
    * twa-cxx (0.0.22) - Template instantiation
    * twa-cxx (0.0.23) - Complete parsing
    * twa-cxx (0.0.24) - Static analysis

* Implement tw-semrep (0.0.26)

* Implement twg-rust (0.0.27)

* Update for changes in tw-semrep
    * twa-c (0.0.28)
    * twa-vb, twa-javascript (0.0.29)
    * twa-html, twa-css, twa-sql (0.0.30)

* Testing / Fixes / Refactoring (0.0.31)

## Pre 0.9

## Pre 1.0

## Post 1.0
